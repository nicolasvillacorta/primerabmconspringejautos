package com.captton.autos.controller;

import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.captton.autos.model.Auto;
import com.captton.autos.repository.AutoDao;


@RestController
@RequestMapping({"/autos"})
public class MainController {
	
	@Autowired
	AutoDao dataauto;
	
	@GetMapping(path= {"/{id}"})
	public ResponseEntity<Object> findId(@PathVariable long id){
		
		JSONObject obj = new JSONObject();
		Auto car = dataauto.findById(id).orElse(null);
		
		if(car!=null) {
			JSONObject carobj = new JSONObject();
			carobj.put("modelo", car.getModelo());
			carobj.put("marca", car.getMarca());
			carobj.put("patente", car.getPatente());
			
			obj.put("error", "0");
			obj.put("result", carobj);
			
			return ResponseEntity.ok().body(obj.toString());
		}else {
			obj.put("error", "1");
			obj.put("result", "No se encontro el ID buscado");
			
			return ResponseEntity.ok().body(obj.toString());
		}
			
	}
	
	@PostMapping
	public Auto create(@RequestBody Auto auto) {
		return dataauto.save(auto);
	}

	@DeleteMapping(path= {"/{id}"})
	public ResponseEntity<Object> delete(@PathVariable ("id") long id){
		
		dataauto.deleteById(id);
		
		return ResponseEntity.ok().body("Registro borrado");
	}
	
	@GetMapping
	public ResponseEntity<Object> findAll(){
		
		JSONObject obj = new JSONObject();
		List<Auto> autosEnc = dataauto.findAll();
		
		if(autosEnc.size()>0) {
			
			for(Auto au: autosEnc)
				System.out.println("Se encontro un "+au.getMarca()+" "+au.getModelo()+" de color "+au.getColor());
			
			obj.put("error", "0");
			obj.put("results", autosEnc);
			
			return ResponseEntity.ok().body(obj.toString());
		}else {
			obj.put("error", "1");
			obj.put("results", "No se encontraron registros");
			return ResponseEntity.ok().body(obj.toString());
		}
	}

	@PutMapping(value= "/{id}")
	public ResponseEntity<Auto> update(@PathVariable("id") long id, @RequestBody Auto auto){
		return dataauto.findById(id).map(record ->{
			record.setMarca(auto.getMarca());
			record.setModelo(auto.getModelo());
			record.setPatente(auto.getPatente());
			record.setColor(auto.getColor());
			
			Auto updated = dataauto.save(record);
			
			return ResponseEntity.ok().body(updated);
			
		}).orElse(ResponseEntity.notFound().build());
	}
	
	@GetMapping(path= {"buscarPat/{patente}"})
	public ResponseEntity<Object> buscarPorPatente(@PathVariable("patente") String patente){
		
		JSONObject obj = new JSONObject();
		List<Auto> autosEnc = dataauto.findByPatente(patente);
		
		if(autosEnc.size()>0) {
			for(Auto car: autosEnc)
				System.out.println("Se encontro con la patente"+patente+" un "+car.getMarca()+" "+car.getModelo()+" de color "+car.getColor());
			
			obj.put("error", "0");
			obj.put("results", autosEnc);
			
			return ResponseEntity.ok().body(obj.toString());
		}else {
			obj.put("error", 1);
			obj.put("results", "No se encontro ningun auto con la patente ingresada");
			
			return ResponseEntity.ok().body(obj.toString());
		}
	}
}
